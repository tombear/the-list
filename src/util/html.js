import { div, span, label, tr, td } from "callbag-html";
import { fromEvent, EMPTY, of, pipe } from "rxjs";
import { pluck, map, tap, mapTo } from "rxjs/operators";

export const freqElement = (emitter, filters) => ([word, count]) => {
  const element =
    div({
      id: word
    }, `${word} (${count})`);

  element.classList.add("freq", filterCheck(word, filters) ? "selected" : "freq");
  element.onclick = () => emitter(pipe(
    map(prev => {
      let { filters, ...rest } = prev;
      let i = filters.selectedWords.findIndex(x => x === word);
      if (~i) {
        return {
          ...rest,
          filters: {
            ...filters,
            selectedWords: [
              ...filters.selectedWords.slice(0, i),
              ...filters.selectedWords.slice(i + 1)
            ]
          }
        };
      } else {
        return {
          ...rest,
          filters: {
            ...filters,
            selectedWords: [...filters.selectedWords, word]
          }
        }
      }
    })
  ));


  return element;
};

function filterCheck(word, { selectedWords = [], fuzzyWord = "" }) {
  return selectedWords.includes(word) ? true : false;
}

export function clearHtml() {
  wordFreqs.innerHTML = "";
  listTable.innerHTML = "";
}

export function entryElement(row) {
  return tr(Object.keys(row).map(k => td(row[k])));
}
