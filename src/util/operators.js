import { from, of, pipe } from "rxjs";
import { map, mergeMap, pluck } from "rxjs/operators";

export const plucks = (...xs) => pipe(
  mergeMap(input => from(xs).pipe(
    mergeMap(x => of(input).pipe(
      pluck(x)
    ))))
);

export const split = by => pipe(
  map(someString => someString.split(by))
);
