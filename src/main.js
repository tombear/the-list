import fromFunction from "callbag-from-function";
import { th, tr } from "callbag-html";
import { csvParse } from "d3-dsv";
import { from, fromEvent, identity, of, ReplaySubject } from "rxjs";
import { debounceTime, distinctUntilChanged, filter, map, mergeMap, reduce, scan, share, startWith, switchAll, switchMap, take } from "rxjs/operators";
import { entryElement, freqElement, plucks, split, toObservable } from "./util";

const URL = "./the-list.csv"
const SPLIT_REGEX = /[\s();,.\/\\]/;

const { source, emitter } = fromFunction();
const source$ = toObservable(source);

const initialState = {
  freqs: [],
  entries: [],
  filters: {
    selectedWords: [],
    columns: ["cause of death"],
    fuzzyWord: ""
  }
}

const bs$ = new ReplaySubject(1);
from(fetch(URL, {
  headers: {
    "Accept": "text/plain",
  }
}).then(response => response.text())).pipe(
  map(text => csvParse(text)),
).subscribe(bs$);

const row$ = bs$.pipe(
  filter(x => !(x === undefined)),
  distinctUntilChanged(),
  switchMap(from),
  map(x =>  (delete x[""], x))
);

const main$ = source$.pipe(
  startWith(_ => of(initialState)),
  scan((acc, v) => v(acc).pipe(
    switchMap(({ filters, filters: { columns, fuzzyWord, selectedWords }, ...rest }) =>
      row$.pipe(
        switchMap(row => of(row).pipe(
          plucks(...columns),
          reduce((acc, v) => (acc + ' ' + v).trim(), ""),
          filter(s => fuzzyWord == "" || s.toLowerCase().includes(fuzzyWord.toLowerCase())),
          split(SPLIT_REGEX),
          filter(ws => selectedWords.length == 0 ? true :
            selectedWords.reduce((acc, v) => {
              return acc && ws.includes(v)
            }, true)),
          switchMap(words => from(words).pipe(
            filter(w => w !== "" && w.length > 2),
            reduce((acc, v) => acc[v] ? (acc[v]++ , acc) : (acc[v] = 1, acc), {}),
            map(freqs => ({ entry: row, freqs }))
          )),
        )),
        reduce((acc, { entry, freqs }) => {
          acc.entries.push(entry);
          Object.keys(freqs).forEach(k => acc.freqs[k] ? acc.freqs[k] += freqs[k] : acc.freqs[k] = freqs[k]);
          return acc;
        }, { entries: [], freqs: {} }),
        mergeMap(({ entries, freqs }) => from(Object.keys(freqs)).pipe(
          reduce((acc, k) => [...acc, [k, freqs[k]]], []),
          map(freqs => ({ entries, freqs: freqs.sort((a, b) => b[1] - a[1]) }))
        )),
        map(({ freqs, entries }) => ({ ...rest, filters, freqs, entries })),
      )),
  ), identity),
  switchAll(),
  share(),
);
// you need to generate a view from the state, with emissions...

const freqElements$ = main$.pipe(
  switchMap(({ freqs, filters }) => of(freqs).pipe(
    map(xs => [...xs].sort(([_, c], [__, c2]) => c2 - c)),
    switchMap(from),
    take(30),
    reduce((acc, v) => [...acc, freqElement(emitter, filters)(v)], []),
  )),
);

const entryElements$ = main$.pipe(
  switchMap(({ entries }) => of(entries).pipe(
    map(rows => [...rows].sort((a, b) => parseInt(a.ID) - parseInt(b.ID))),
    switchMap(from),
    // take(10),
    reduce((acc, row) => [...acc, entryElement(row)], []),
    // takeUntil with windowing/buffering to do scrolly thing
  )),
)

freqElements$.subscribe(xs => {
  wordFreqs.innerHTML = "";
  xs.forEach(x => wordFreqs.appendChild(x));
});

entryElements$.subscribe(xs => {
  listTable.innerHTML = "";
  listTable.appendChild(tr(["ID", "date", "number", "name, gender, age", "region of origin", "cause of death", "source"].map(x => th(x))))
  xs.forEach(x => listTable.appendChild(x));
})

// const searchBox = input({
//   type: "text",
//   id: "searchBox"
// });

// root.insertBefore(searchBox, wordFreqs);

fromEvent(searchBox, "input").pipe(
  debounceTime(1000),
  distinctUntilChanged(),
).subscribe(text => emitter(prev$ => prev$.pipe(
  map(({ filters, ...rest }) => ({
    ...rest, filters: {
      ...filters,
      fuzzyWord: text.target.value
    }
  }))
)));
